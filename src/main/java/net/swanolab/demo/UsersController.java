package net.swanolab.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/user")
public class UsersController {
    @Autowired
    private UsersRepository userRepository;

    @PostMapping("/add")
    public @ResponseBody String addNewUser(@RequestParam String name, @RequestParam String email){
        Users n = new Users();
        n.setEmail(email);
        n.setName(name);

        userRepository.save(n);

        return "Saved";
    }

    @GetMapping("/all")
    public @ResponseBody Iterable<Users> getAllUsers(){
        return userRepository.findAll();
    }
}
